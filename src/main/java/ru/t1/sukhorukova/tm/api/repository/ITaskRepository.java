package ru.t1.sukhorukova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnerRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}
